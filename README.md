# README #

### What is this repository for? ###

The repo hosts the source code of the Websource SDK. The Websource SDK is a very simple interface responsible for allowing developers to create externally hosted webpages that can be used as web source inputs in Cloud Producer, allowing them to respond to events such as a source going live or receiving a data update.


### How do I get set up? ###

To setup pull the repo and run `npm install`. After the dependencies are installed, you can run the following:

To compile the source into an exportable format:
```
npm run build
```
To build documentation 
```
npm run build-docs
```

TODO:

To push to S3 (?)
```
npm run deploy
```
To deploy docs 
```
npm run deploy-docs
```