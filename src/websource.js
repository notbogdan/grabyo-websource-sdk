/**
 * Websource constants
 * @namespace
 * @property {string} ON_LIVE 	- On live event - "live"
 * @property {string} OFF_LIVE	- Off live event - "offLive"
 * @property {string} ON_DATA	- On live event - "data"
 * @exports constants
 */
export const constants = {
	ON_LIVE: "live",
	OFF_LIVE: "offLive",
	ON_DATA: "data"
};

/**
 * Websource default instance.
 * @namespace
 * @property {boolean} isLive 	- Flag identifying if websource is live
 * @property {function} on 		- Attach a websource event listener.
 * @property {function} off 	- Remove a previously attached event listener by name and callback or just by name (removes all matching events).
 * @property {function} trigger - Triggers bound events by eventName, passing parameter into callback.
 * @exports default
 */
const websource = {
	isLive: false,
	definedEvents: [],

	/**
	 * @param {String} eventName
	 * @param {Function} callback
	 */
	on: function (eventName, callback) {
		if (typeof eventName !== "string" || Object.values(constants).indexOf(eventName) < 0) {
			console.error("Invalid event name passed to websource event handler.");
			return;
		}
		if (typeof callback !== "function") {
			console.error("Invalid callback passed to websource event handler.");
			return;
		}
		this.definedEvents = [...this.definedEvents, {
			eventName,
			callback
		}];
	},

	/**
	 * @param {String} eventName
	 * @param {Function} callback
	 */
	off: function (eventName, callback) {
		if (typeof eventName !== "string") {
			console.error("Invalid event name passed to websource event handler.");
			return;
		}
		this.definedEvents = this.definedEvents.filter(el => {
			if (typeof callback === "function") {
				return el.eventName !== eventName || el.callback !== callback;
			} else {
				return el.eventName !== eventName;
			}
		});
	},

	/**
	 * @param {String} eventName
	 * @param {Object} param
	 */
	trigger: function (eventName, param = {}) {
		if (typeof eventName !== "string") {
			console.error("Invalid argument passed to websource event trigger");
		}
		this.definedEvents.forEach(({ eventName: name, callback }) =>
			eventName === name ? callback(param) : null
		);
	}
};

export default websource; 

// Chromium engine callees

const tryParseJSON = jsonString => {
    try {
        var o = JSON.parse(jsonString);
        if (o && typeof o === "object") {
            return o;
        }
    } catch (e) { 
		console.error("There has been a problem parsing the JSON supplied to setData or onLive.");
	}

    return false;
};


window.setData = function (data) {
	const parsedData = tryParseJSON(data);
	if(parsedData){
		websource.trigger(constants.ON_DATA, parsedData);
	}
}

window.onLive = function (data) {
	const parsedData = tryParseJSON(data);
	if(parsedData){
		websource.trigger(constants.ON_LIVE, parsedData);
		websource.isLive = true;
	}
}

window.offLive = function () {
	websource.trigger(constants.OFF_LIVE);
	websource.isLive = false;
}