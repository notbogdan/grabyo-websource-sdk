const gulp = require('gulp');
const babel = require('gulp-babel');
const jsdoc = require('gulp-jsdoc3');
const runSequence = require('run-sequence');

/**
 * Transpile to ES5, create UMD module and minify
 */
gulp.task('babel', () =>
	gulp.src('src/websource.js')
		.pipe(babel({
			plugins: ['babel-plugin-transform-es2015-modules-umd'],
			presets: ['env', 'minify']
		}))
		.pipe(gulp.dest('dist'))
);

// Define tasks

gulp.task('build', ['babel']);
